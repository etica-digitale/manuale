# Canali Sociali e Contatti

- Linkstack (alternativa libera a linktree): https//link.eticadigitale.org

## Canale Telegram
- @eticadigitale

## Gruppo pubblico di Telegram 
- @eticadigitalegruppo


## Pagina Instagram
- @eticadigitale


## Pagina Mastodon
- @eticadigitale@mastodon.bida.im
- Mastodon.bida.im è una istanza Mastodon, alternativa libera simile a Twitter/Threads, gestita dal collettivo antifascista bida.im


## Canale PeerTube
- @eticadigitale@peertube.uno
- PeerTube.uno è una istanza PeerTube, alternativa libera simile a Youtube, gestita dai Devol

## forum su Feddit
- @eticadigitale@feddit.it
- Feddit è un istanza Lemmy, un social network libero alternativo a Reddit, gestito dai nostri amici di LeAlternative e InformaPirata

## email
- etica.digitale@mailfence.com


## FAQ

> Dove trovo le password?

Chiedi sul gruppo interno


> Perché per la mail usate il servizio Mailfence e non Protonmail?

etica.digitale@protonmail.com non è più disponibile per errori umani che abbiamo fatto, abbiamo provato a registrare etica.digitale@proton.me ma non è altrettanto disponibile.