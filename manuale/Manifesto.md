# Manifesto
Etica Digitale è un gruppo di attiviste e attivisti che si occupa di riportare l’individuo e i diritti al centro del dibattito tecnologico.

Lo facciamo tramite la divulgazione, la ricerca e la creazione di spazi digitali dove discutere di queste tematiche. Miriamo ad un pubblico ampio, variegato e non specializzato.

Vogliamo mettere in discussione una visione tecnosoluzionista della tecnologia che ha causato ( e sta causando) grossi danni, che ha permesso la centralizzazione dei processi digitali in mano a poche grosse aziende che si stanno rendendo responsabili di grosse violazioni dei diritti umani, danni all’ambiente e alla salute mentale delle persone.

## Tematiche che trattiamo

- Privacy;
- Impatto delle tecnologie sulla psiche, sulla società e sull’ambiente;
- Asimmetria di potere tra chi produce o sviluppa la tecnologia (aziende, governi) e chi la utilizza;
- Impatto del digitale sull’autodeterminazione delle persone;
- Filosofia, politica e regolamentazione del digitale;
- Software libero;
- Diritto alla riparazione;
- Intelligenza artificiale;
- Criptovalute, Web3.0 e NFT e le loro criticità;
- Etica dei videogiochi.

## Che cosa facciamo

Divulghiamo notizie, articoli e saggi preferendo la qualità alla quantità. Viviamo in un epoca in cui siamo bombardati di notizie, se dopo averne letta una ci arrivano altre 10 notifiche con nuove informazioni, allora non rimane alcuno spazio mentale per riflettere su ciò che abbiamo letto e la notizia finirà a breve nel dimenticatoio.

Organizziamo e partecipiamo a incontri formativi, seminari e incontri di lettura.

Abbiamo creato e manteniamo degli spazi digitali di discussione, confronto e comunità come il nostro gruppo Telegram e il nostro server Minetest.

Stiamo scrivendo PrivaSì, la nostra guida alla privacy, che mira a guidare passo per passo i lettori verso una scelta più consapevole delle proprie applicazioni e servizi digitali.
