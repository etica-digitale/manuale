# Lista dei gruppi di lavoro

!!! note "Attenzione" 
    
    La lettura di [Governance](Governance.md) è propedeutica, senza la quale si rischia di non capire che ruolo hanno i gruppi di lavoro nella nostra organizzazione.


## Gruppi di lavoro permanente

- Infrastruttura IT: si occupano di manutenere i servizi già esistenti (come la nuvola, il sito) e di inserirne altri - richiede conoscenze sistemistiche e di rete;

- [Riassunti](RiassuntiCanale.md) sul canale Telegram: si occupano di fare riassunti di articoli che vengono pubblicati sul canale;

- [Canali Social](CanaliSociali.md): si occupano di sponsorizzare il lavoro attraverso i nostri canali sociali e di produrre contenuti e [grafiche](GraficheSocial.md) da mettere sui social;


## Gruppi di lavoro temporanei (potrebbe non essere aggiornato spesso)

- PrivaSì: il nostro progetto principale, la guida alla privacy;

- Ogni volta che veniamo invitati a qualche evento, viene creato informalmente un gruppo di lavoro apposito;