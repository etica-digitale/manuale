# Grafiche Social
Di cosa hai bisogno:
1. Email e password per l'account [Canva](https://www.canva.com/) di Etica Digitale
2. Un sito per controllare il contrasto dei colori, tipo [Color Contrast Checkers](https://coolors.co/contrast-checker/112a46-acc8e5) di Colors
3. (Se non lo sai già fare) Un [tutorial](https://youtu.be/D126KH5e2k0?si=lGpbSk4xN6inblQ3) su come fare i caroselli di Instagram + ([separa immagini di Pinetools](https://pinetools.com/split-image)


## (DA FARE) Formato standard per i post


## Palette
Ispirazioni:
- [The Verge](https://www.theverge.com/2022/9/13/23349876/the-verge-website-redesign-new-newsfeed-blogs-logo)
- [Proton](https://proton.me/blog/new-visual-universe)

Siti utili per trovare palette:
- [Color Hunt](https://colorhunt.co/)
- Canva ha una funzione integrata che ti suggerisce i colori


Palette 1:
- #2ee5c1
- #4116a2

Palette 2:
- #f3eac0
- #2f4858
- #551a25