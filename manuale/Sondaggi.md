# Sondaggi

!!! important "Attenzione" 

    La lettura di [Governance](Governance.md) è propedeutica, senza la quale si rischia di non capire che ruolo hanno le riunioni nella nostra organizzazione.

## Perché si fanno i sondaggi 
Rileggi [Governance](Governance.md)

## Quando si fanno i sondaggi
Possibilmente si preparando nella riunione stessa, talvolta anche il giorno dopo.

## Dove si fa il sondaggio 
Nell'apposito [canale interno](Strumenti.md#guida-alla-chat-telegram-interna-del-collettivo) "Sondaggi" e poi si inoltra in "Importante". Assieme al sondaggio va allegato il razionale prodotto a [riunione](Riunioni.md#come-si-scrivono-i-verbali)


## Come si fa il sondaggio
Noi preferiamo seguire principi democratici e scientifici nei sondaggi:

- Il sondaggio è sempre anonimo;

- Il sondaggio deve sempre contenere l'opzione per **astenersi**;

- Se bisogna scegliere COME fare qualcosa, il sondaggio deve sempre contere l'opzione **Altro (scrivi)** che NON sostituisce l'opzione per **astenersi**;

- Il sondaggio deve sempre avere un "Razionale" che spieghi il contesto, le visioni emerse a [riunione](Riunioni.md) e alcune sfide e conseguenze di una cosa del genere

## Dopo quanto tempo viene chiuso un sondaggio

Il sondaggio viene chiuso all'assemblea successiva, così che prima di iniziare si può dare uno sguardo a quello discusso la scorsa volta.