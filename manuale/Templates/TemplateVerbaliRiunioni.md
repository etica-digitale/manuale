# Verbale riunione dd-mm-yyyy <-- Inserisci qui la data
**Presenti:** Tizio, Caio,...
**Data e ora:** dd-mm-yyyy 18:30

## Ordine del giorno
- [ ] Punto 1
- [ ] Punto 3.4
- [ ] ...

## Discussioni

### Punto 1

### Punto 3.4

### ...


## Cose da fare
- ...


