# Governance

Etica Digitale è un collettivo indipendente, fortemente orizzontale, pragmatico e flessibile.

- Indipendente: non siamo finanziati da grossi gruppi di interesse;

- Fortemente: orizzontale significa che non esistono gerarchie interne. Il "fortemente" è perché la piena orizzontalità è impossibile in pratica;

- Pragmatico: preferiamo approcci che portino a dei risultati concreti;

- Flessibile: se vediamo che una strada non porta da nessuna parte allora non abbiamo problemi a cambiarla.

Tendiamo a fare lavoro (volontario) in modalità asincrona, dato che ad oggi il collettivo è sopratutto online con qualche evento dal vivo. Affinché il lavoro resti volontario, nessunə attivistə è costrettə a fare nulla, nemmeno per "continuità", dato che ognunə qui spende (e forse investe) il proprio tempo come preferisce.

## Decisioni del collettivo

### Come si effettuano le decisioni

L'assemblea discute i punti e propone delle soluzioni o misure da attuare. Queste vengono votate in assemblea se è molto partecipata (>=75%), altrimenti si mettono ai voti tramite i [sondaggi](Sondaggi.md). 

I sondaggi si fanno sempre di default, poi l'assemblea decide cosa proporre nel sondaggio e valuta i singoli casi (rari) in cui non si fa il sondaggio. 

Durante una discussione possono emergere diversi punti di vista, noi preferiamo trovare una sintesi e nel caso non si dovesse trovare, si passa alle votazioni e la maggioranza vince.


### Eccezioni
#### Revoca di decisioni

Le decisioni possono essere revocate dall'assemblea nel caso in cui a posteriori ci si rende conto che non portino al risultato sperato, siano controproducenti o dannose.


## Gruppi di lavoro

- Vedi [la lista dei gruppi di lavoro](GruppiDiLavoro.md)

### Progetto
Un progetto è un insieme di task raggruppati con lo scopo di restituire qualcosa alla collettività direttamente o indirettamente.
Dato che siamo convinti che undendo le forze si riescono a creare prodotti qualitativamente migliori, tendiamo a creare dei gruppi di lavoro (temporanei o permanenti), composti principalmente da attivistə del collettivo che sono interessatə a spendere un po' del loro tempo per qualcosa da restituire alla collettività.

Un esempio di gruppo di lavoro permanente è quello dei canali sociali che produce contenuti.

Un esempio di gruppo di lavoro temporaneo è stato quello per la ristrutturazione del sito web o per il laboratorio presentato a Scambi 2022

### Caratteristiche di un gruppo di lavoro

- Lə attivistə che fanno parte di un gruppo di lavoro hanno piena autonomia all'interno di questo e possono autogestirsi come meglio preferiscono;

- I gruppi di lavoro sono fluidi, non c'è alcuna formalità nell'entrata e nell'uscita, non è richiesta la partecipazione assidua e generalmente si può partecipare in maniera occasionale;

- Il lavoro fatto va documentato e le conoscenze vanno condivise in modo da poter essere ripreso in futuro da questo o da altri gruppi;

- Il lavoro fatto è trasparente all'interno del collettivo, gli esterni possono [suggerire cambiamenti](BestPractices.md#buone-pratiche-per-dare-feedback) ma l'ultima parola è degli interni. ([scopri perché](#domande-frequenti))


### Una volta terminato il lavoro...
Viene proposto sul gruppo interno al resto del collettivo e poi viene approvato. I passi successivi poi dipendono dal progetto, in genere viene sponsorizzato sui [canali sociali](CanaliSociali.md).


### Riunioni e Feedback
Di solito nei gruppi di lavoro c'è la tendenza a fare delle riunioni apposite, tuttavia non è strettamente necessario. Per esempio il gruppo di lavoro sui riassunti non ha mai fatto riunioni. 

Per chiedere feedback e far vedere i progressi (opzionale) si può usare sia la [chat interna](Strumenti.md#guida-alla-chat-telegram-interna-del-collettivo) che le [riunioni del collettivo](Riunioni.md).


### Perché i gruppi di lavoro

- Nella maggior parte dei casi ci troviamo a fare lavoro asincrono, tuttavia talvolta è necessario un confronto sincrono o darsi delle scadenze;

- Unendo le forze si riesce a fare di più e a produrre risultati qualitativamente migliori;

- Tendiamo a preferire che persone competenti (o vogliose di imparare) in un ambito si occupino di un progetto inerente a quello. Il gruppo di lavoro tende a raggruppare naturalmente persone con competenze "specifiche";

- I gruppi di lavoro creano elementi di verticalità che aumentano l'efficienza: immagina se bisognasse aspettare che tutti i membri votassero si o no per aggiungere una nuovo bottone al sito, il sito non cambierebbe mai. 

- Lə attivistə possono essere proriamente accreditate per il loro lavoro.



### Eccezioni
Alcuni gruppi di lavoro come quello che mantiene l'infrastruttura IT per ovvi motivi non possono seguire tutte le pratiche elencate qui.


## Etica Digitale
Etica Digitale si impegna ad utilizzare software libero e open source, social network liberi e di rilasciare tutto quello prodotto con licenze CC-BY-SA (Creative Commons) e L/GPL 3.0 - le eccezioni sono dovute (a malincuore) a compromessi da fare con la realtà dei fatti. Ad esempio, il motivo per cui ci troviamo su [Instagram](CanaliSociali.md#pagina-instagram) è nato dal compromesso di raggiungere più persone che spesso non usano Mastodon e non hanno una forte cultura tecnica, e di conseguenza non seguono alcuni temi che sono considerati di nicchia come appunto il software libero o il diritto alla riparazione.


### Domande frequenti e chiarimenti

> Perché chi fa parte di un gruppo di lavoro ha l'ultima parola sul lavoro fatto? E allora se il lavoro fatto non lo reputo sufficientemente buono?

Dato che non esiste alcuna formalità per entrare in un gruppo di lavoro, il miglior modo per non far uscire lavori che fanno schifo è partecipare in prima persona a quel gruppo, anche dando consigli, condividendo conoscenze o facendo lavoro occasionale. Questi sono tutti metodi che possono contribuire a migliorare la qualità del lavoro senza richiedere troppo tempo da dedicare.

Distruggere il lavoro di qualcuno che si è impegnato, sopratutto se volontario, distrugge l'entusiasmo, disincentiva la partecipazione e spinge a fare le cose da soli, il che fa perdere il senso di avere un collettivo.

---

> Non riesco ad esserci spesso a riunione o a seguire la chat ma voglio contribuire, come posso fare?

- Dai feedback sul lavoro fatto da altri, ma [assicurarti di farlo in modo corretto](BestPractices.md#buone-pratiche-da-seguire)!

- Nei periodi in cui sei più scaricə, puoi contribuire in modo occasionale, lavorando sopratutto in modo asincrono non è troppo difficile farlo, per qualsiasi dubbio chiedete in chat e vi verrà dato;

- Se te lo puoi permettere, puoi effettuare una [donazione](Donazioni.md), solitamente siamo in perdita, il nostro obiettivo è pareggiare spese e donazioni annuali;

- Introduci il nostro lavoro ad amici, conoscenti, altri collettivi e a persone possibilmente interessatə.


---


> Vi chiamate Etica Digitale ma fate {inserisci cosa con cui non sono d'accordo} che non è tanto "etica"

Anzitutto bisogna chiarire la distinzione tra [morale](https://it.wikipedia.org/wiki/Morale) ed [etica](https://it.wikipedia.org/wiki/Etica). Quando ci viene posta questa domanda solitamente è perché abbiamo fatto qualcosa con cui o non sei d'accordo, oppure non rientra nella tua morale. 

Spesso la realtà è complessa e ci pone di fronte a scelte che non sono bianche o nere, ma grigie. Bisogna inoltre considerare che ED è composta da individui con opinioni anche diverse su alcuni argomenti e spesso le decisioni sono dei compromessi tra queste visioni. 

Accettiamo le critiche, ma vi prego di non basarvi sul nostro nome ma su quello scritto nel [manifesto](Manifesto.md), grazie.

--- 