# Benvenutə sul manuale di Etica Digitale

## Da dove iniziare

!!! note "Nota" 

    Questo manuale è pensato per le attivistə che appartengono al collettivo, ma non ci sono informazioni sensibili, quindi se sei un esternə e lo stai leggendo non ti mangeremo :3
    
    
!!! tip "Curiosità"

    Ci siamo ispirati al manuale di [Scambi](https://manuale.scambi.org/)


Per iniziare, è fortemente consigliata la lettura di [Governance](Governance.md) e [Manifesto](Manifesto.md).

Nella barra affianco leggi gli argomenti che ti interessano, oppure utilizza la barra di ricerca. 