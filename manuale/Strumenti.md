# Strumenti

## Nuvola 
La nuvola è il nostro cloud interno, basato su software libero Nextcloud, dove mettiamo tutti i file che vanno condivisi come i verbali delle [riunioni](Riunioni.md), le grafiche, le palette utilizzate e così via. 
Nextcloud è una suite moderna completa, quindi è dotato anche di calendario, foto, consente di vedere e modificare file, spreadsheet e così via. 


## Guida alla chat Telegram interna del collettivo 

!!! warning "Attenzione" 

    Da non confondere con la chat Telegram pubblica @eticadigitalegruppo

Per chi ha poco tempo da dedicare alla chat, ricordiamo che può mutare le notifiche di tutti i canali oppure di quelli a cui non è interessatə. 

- Importante: qui vanno tutte le comunicazioni importanti che non stanno negli altri canali. Come regola generale se è qualcosa che tuttɜ dovrebbero leggere o in breve tempo, può essere messa qui;

- Sondaggi: è una appendice della chat precedente, perfavore leggi [governance](Governance.md);

- PrivaSì: gruppo di lavoro di privasi.eticadigitale.org;

- Articoli: gruppo di lavoro degli articoli, si possono proporre articoli fino ad un massimo di 5 fissati in alto, proporre le bozze dei riassunti e discutere se sono opportuni da mettere o meno sul canale. I messaggi vengono cancellati dopo che un riassunto viene pubblicato;

- Eventi: qui vanno le discussioni e informazioni inerenti agli eventi a cui partecipiamo;

- Reti Sociali: gruppo di lavoro sui canali sociali: Instagram, Mastodon, Telegram;

- Traduzioni: gruppo di lavoro sulle traduzioni di materiale in Italiano.

- Generale & Off-topic: qui potete litigare va tutto l'off-topic e quello che non è strettamente inerente alla nostra attività di divulgazione e attivismo;

La chat è uno strumento organizzativo, perfavore teniamola pulita e utilizziamo gli appositi topic, così che chiunque possa seguire e contribuire alle conversazioni.


## Gitlab
[Gitlab](https://gitlab.com/etica-digitale) è la piattaforma che utilizziamo per lavorare con progetti che comprendono sia documenti scritti che codice (per le automazioni, come ad esempio le [Presentazioni](https://presentazioni.eticadigitale.org) o [PrivaSì](https://privasi.eticadigitale.org) )


## Lista - Raccoglitore di notizie 

!!! tip "Curiosità"

    Il nome è un meme nato nella chat interna, "lista" dovrebbe stare per lista di fonti, in pratica è un raccoglitore di articoli

- Potete trovarlo al link https://lista.eticadigitale.org
- Viene aggiornato in automatico ogni giorno, finché le fonti continueranno a pubblicare notizie (e non ci sono problemi tecnici) allora verranno introdotte nuove notizie
- Solitamente la lista viene usata per fare i riassunti da pubblicare sul canale
- Lista non è mai stata annunciata pubblicamente sul canale Telegram, ma l'accesso è pubblico.
- Le notizie sono categorizzate per argomento e per tipologia di fonte (esempio: articoli in inglese, articoli in italiano, software libero, AI etc)