# Meta 

## Principi per scrivere un buon manuale operativo

- Non dare mai nulla per scontato

- Non assumere che le altre persone abbiano il tuo stesso livello di competenza e conoscenza

- Seguire la struttura Cosa, Dove, Come e Quando. Il perché di qualcosa è spiegato solitamente in [Governance](Governance.md)

- Tenere aggiornato il manuale, altrimenti smette di essere un punto di riferimento

- Copri i corner case


## Perché abbiamo bisogno di un manuale
Il Manuale di Etica Digitale è stato concepito per una serie di necessità:

- Avere uno spazio ordinato che spieghi come funzionano i processi interni: decisioni, burocrazia, strumenti che utilizziamo, il perché e così via;

- Mettere per iscritto i processi decisionali ci aiuta a farli rispettare e a rimanere coerenti;

- Utilizziamo quasi esclusivamente software libero e open source, spesso questi strumenti sono più complicati di quelli tradizionali, da qui la necessità di suggerire come usarli e rispondere a domande e dubbi comuni;

- Condividere e formare lə attivistə sulle "best practices" organizzative.


## Cosa NON va messo dentro il manuale
- I verbali delle [riunioni](Riunioni.md), il motivo è che non in tutte le riunioni si discute di cambiamenti nella struttura interna o si cambiano/aggiungono nuovi strumenti;

- Informazioni sensibili di qualsiasi tipo poiché il manuale è pubblico;

- Tutto ciò che non è "operativo", per esempio gli articoli o le iniziative fatte da Etica Digitale non vanno messe qui.