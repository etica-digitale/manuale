# Riunioni

!!! important "Attenzione" 

    La lettura di [Governance](Governance.md) è propedeutica, senza la quale si rischia di non capire che ruolo hanno le riunioni nella nostra organizzazione.


## Cosa devi sapere sulle riunioni (se hai letto [Governance](Governance.md))
- Le riunioni non sono obbligatorie tuttavia la partecipazione è fortemente incoraggiata;
- Per continuità le riunioni si fanno sempre, salvo diversamente comunicato per cause di forza maggiore o vacanze;
- Se non ci sono punti da discutere, lə attivistə sono libere di fare come vogliono, solitamente si fa una riunione più breve, più leggera dove si chiacchiera.


## Cosa utilizziamo per le riunioni
Utilizziamo [Jitsi](https://meet.jit.si/) una piattaforma di videoconferenze simile a Zoom, ma a differenza di questa libera e rispettosa della privacy. 

Non bisogna scaricare nulla, si può accedere da browser o da app se preferite, basta cliccare sul link che viene comunicato nella [chat interna](Strumenti.md#guida-alla-chat-telegram-interna-del-collettivo) prima di ogni riunione.

## Quando facciamo le riunioni
Ogni due martedì, solitamente dalle 18:30, e hanno una durata circa di 1 ora o ore e mezza.

## Ordine del giorno
E' la lista di punti da discutere.

Ogni attivistə può aggiungere un punto all'ordine del giorno seguendo il proprio giudizio (e talvolta, intuito). E' buona norma comunicare in anticipo, anche in chat se si vuole aggiungere di un punto e non presentarsi in riunione con qualcosa di nuovo. Questo perché così si da il tempo allə altrə di cercare materiale o prepararsi (in tutti i sensi) ad una discussione.

I punti che non si riesce a discutere per mancanza di tempo vengono slittati alla riunione successiva.


## Verbale
Le discussioni fatte in riunione vengono brevemente riportate a verbale, riportando tutti i punti di vista ed eventuali sintesi. 

### Dove si trascrivono i verbali
Sulla [nuvola](Strumenti.md#nuvola), nella barra in alto: Collettivi > Verbali delle riunioni > Sulla sinistra, clicca sul + > Si apre un documento sul quale puoi scrivere liberamente. Il file va rinominato seguendo il formato: Giorno-Mese-Anno, per esempio: 18-07-2023, 04-07-2023 etc.

!!! note "Nota" 

    Stiamo cercando una soluzione migliore per trascrivere i verbali


Se vengono effettuati cambiamenti [operativi](index.md) allora vanno riassunti e trascritti anche nel manuale. 

### Come si scrivono i verbali

1. Utilizza il template [Template Verbali Riunioni](Templates/TemplateVerbaliRiunioni.md), copialo e aggiungo nella cartella Template su Nextcloud
2. Nomina il file con la data odierna nella cartella `Verbali Riunioni/2023`
3. Riporta i punti di vista di tuttə e il nome dell'attivistə come preferisci nella sezione "Discussioni"
4. Aggiungi le cose da fare che si sono decise a riunione nella sezione in basso, inclusi i [sondaggi](Sondaggi.md)