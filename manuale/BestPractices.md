# Buone pratiche da seguire


## Buone pratiche per dare feedback

- Deve essere dato al momento giusto, ne troppo tardi a distanza di mesi, ne troppo presto;

- Deve essere specifico e circoscritto;

- Deve essere fatto lavoro o al comportamento e non alla persona (altrimenti potrebbe essere inteso come un attacco personale);

- Deve avere l'obiettivo di migliorare il lavoro fatto o correggere un comportamento;

- Deve usare un linguaggio neutro che non risulti offensivo o che non mini all'autostima dell'altra/altre persone;

- Deve essere obiettivo, basandosi su esempi concreti e sottolineare l'impatto che può avere;


### Che differenza c'è tra il feedback e il giudizio?
Il giudizio è sentenziato in maniera univoca e non necessariamente produce effetti.

Il feedback coinvolge attivamente l'altra persona, ha l'obiettivo di produrre un effetto: migliore o correggere il lavoro fatto da altri. 




## Buone pratiche per le riunioni (Lavori in corso)

## Buone pratiche da seguire in chat (Lavori in corso)